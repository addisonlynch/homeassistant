import hassapi as hass


class TimedLight(hass.Hass):

    def initialize(self):
        self.turn_on("light.upstairs_hallway_107")
        self.handle = None

        if "light" in self.args:
            self.listen_state(self.lighton, self.args["light"])
        else:
            self.log("No light specified, doing nothing.")

    def lighton(self, entity, attribute, old, new, kwargs):
        if new == "on":
            self.log("LIGHT TURNED ON")
            delay = self.args.get("delay", 30)
            self.run_in(self.light_off, delay)

    def light_off(self, kwargs):
        self.log("Turning {} off".format(self.args["light"]))
        self.turn_off(self.args["light"])
