import hassapi as hass

import datetime

class OnWithMotion(hass.Hass):

    def initialize(self):
        self.handle = None

        self.handle_args()

        # Set delay
        if "delay" in self.args:
            self.delay = self.args["delay"]
        else:
            self.delay = 0

        # Subscribe to sensors
        if "sensor" in self.args:
            self.listen_state(self.motion, self.args["sensor"])
        else:
            self.log("No sensor specified, doing nothing")

    def handle_args(self):
        self.entities_list = self.args["entities"].replace(" ", "").split(",")

    def motion(self, entity, attribute, old, new, kwargs):
        self.log("Motion from {}".format(self.args["sensor"]))
        if new == "on":
            self.handle = self.run_in(self.light_on, self.delay)
        if new == "off":
            self.handle = self.run_in(self.light_off, self.delay)

    def light_on(self, kwargs):
        self.log("Turning {} on".format(self.entities_list))
        for entity in self.entities_list:
            self.turn_on(entity)

    def light_off(self, kwargs):
        self.log("Turning {} off".format(self.entities_list))
        for entity in self.entities_list:
            self.turn_off(entity)