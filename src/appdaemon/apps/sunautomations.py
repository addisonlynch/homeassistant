import hassapi as hass

from datetime import timedelta


class SunHandler(hass.Hass):

    def initialize(self):
        
        offset = timedelta(minutes=30).total_seconds()
        self.run_at_sunset(self.sunset_lights_on, offset=-offset)
        self.run_at_sunrise(self.sunrise_lights_off, offset=offset)

    def sunset_lights_on(self, kwargs):
        hass.turn_on("switch.left_lamp_80")
        hass.turn_on("switch.right_lamp_81")
        hass.turn_on("light.front_door_74", brightness=40)
        hass.turn_on("light.front_sconces_76", brightness=50)
        hass.turn_on("light.garage_doors_99", brightness=40)
        hass.turn_on("switch.lr_wall_plug_140")

    def sunrise_lights_off(self, kwargs):
        hass.turn_off("switch.left_lamp_80")
        hass.turn_off("switch.right_lamp_81")
        hass.turn_off("light.front_door_74")
        hass.turn_off("light.front_sconces_76")
        hass.turn_off("light.garage_doors_99")
        hass.turn_off("switch.lr_wall_plug_140")