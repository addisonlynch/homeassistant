import hassapi as hass

import datetime

class MotionLights(hass.Hass):

    def initialize(self):
        self.handle = None

        # Check some Params
        self.handle_args()

        # Set delay
        if "delay" in self.args:
            self.delay = self.args["delay"]
        else:
            self.delay = 180

        # Subscribe to sensors
        if "sensor" in self.args:
            self.listen_state(self.motion, self.args["sensor"])
        else:
            self.log("No sensor specified, doing nothing")

    def handle_args(self):
        self.entities_list = self.args["entities"].replace(" ", "").split(",")

    def motion(self, entity, attribute, old, new, kwargs):
        if new == "off":
            self.log("No motion. Timer starting.")
            self.handle = self.run_in(self.light_off, self.delay)
        if new == "on":
            if self.handle is not None:
                self.log("Light turned on. Cancelling previous timer.")
                self.cancel_timer(self.handle)

    def light_off(self, kwargs):
        self.log("Turning {} off".format(self.args["entities"]))
        for entity in self.entities_list:
            self.turn_off(entity)