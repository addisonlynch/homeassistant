#! /usr/bin/env python

# Uses namp to scan the list of active devices on the host network
# Returns a pandas DataFrame of the devices names and statuses

# Requirements:
#
# python-nmap (pip)
# pandas

import argparse
import json
import nmap
import pandas as pd


DEFAULT_HOSTS = '192.168.0.1/24'


parser = argparse.ArgumentParser(description="List devices")
parser.add_argument('--hosts', help='list hosts to scan')
parser.add_argument('-a', '--active', dest='active', help='specifies active '
                    'hosts only', action='store_true')
parser.add_argument('-l', '--list-all', dest='list_all', help='pass flag to '
                    'include IPs which are not assigned', action='store_true',
                    default=False)
parser.add_argument('-o', '--output-file', dest='output_filename',
                    help="Specify filename and extension for output file",
                    default='print')
args = parser.parse_args()


# Initialize the port scanner
scanner = nmap.PortScanner()


def get_all_hosts(format=True):
    active_flag = '-sL' if args.active is False else '-sP'
    results = scanner.scan(args.hosts or DEFAULT_HOSTS, arguments=active_flag)
    return _convert_output(results) if format is True else results


def get_all_hosts_verbose():
    all_hosts = get_all_hosts()
    active_hosts = scanner.scan(args.hosts or DEFAULT_HOSTS, arguments="-sP")
    active_hosts = active_hosts["scan"]

    def _mapper(x):
        if x in active_hosts:
            return "Up"
        else:
            return "Down" if all_hosts.loc[x]["Hostname"] != '' else ''

    all_hosts["Status"] = all_hosts.index.map(_mapper)
    return _out(all_hosts)


def _convert_output(data):
    data = data["scan"]
    results = {key: value["hostnames"][0]["name"]
               for key, value in data.items()}
    df = pd.DataFrame.from_dict(results, orient='index', columns=["Hostname"])
    return df[df["Hostname"] != ''] if args.list_all is False else df


def _out(data):
    filename = args.output_filename
    if filename.endswith('.json'):
        f = open(filename, 'w')
        f.write(json.dumps(data.to_dict(orient='index')))
        f.close()
        return
    elif filename.endswith(('.xlsx', '.xls')):
        return data.to_excel(filename)
    elif filename.endswith('.csv'):
        return data.to_csv(filename)
    else:
        print(data)


if __name__ == "__main__":
    get_all_hosts_verbose()
